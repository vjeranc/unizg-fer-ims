//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package hr.fer.zemris.ims.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;

public class AIClassLoader extends ClassLoader {
    URLClassLoader loader = null;

    AIClassLoader(String[] classPaths, ClassLoader parent) {
        super(parent);
        ArrayList classURLs = new ArrayList();

        for(int i = 0; i < classPaths.length; ++i) {
            String path = classPaths[i];

            try {
                URL ex = new URL(path);
                classURLs.add(ex);
            } catch (MalformedURLException var7) {
                MessageCollector.putError("AIClassLoader", "Bad url: " + path);
            }
        }

        if(!classURLs.isEmpty()) {
            this.loader = new URLClassLoader((URL[])((URL[])classURLs.toArray(new URL[1])), parent);
        }

    }

    public Class loadClass(String classURL) throws ClassNotFoundException {
        Class c = null;
        if(this.loader != null) {
            try {
                c = this.loader.getParent().loadClass(classURL);
            } catch (ClassNotFoundException var6) {
                try {
                    c = this.loader.loadClass(classURL);
                } catch (ClassNotFoundException var5) {
                    throw new ClassNotFoundException("Error while loading class " + classURL, var5);
                }
            }
        } else {
            c = this.getParent().loadClass(classURL);
        }

        return c;
    }
}
