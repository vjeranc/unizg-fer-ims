//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package hr.fer.zemris.ims.utils;

import com.jme3.scene.Geometry;
import java.util.ArrayList;
import java.util.Iterator;

public class WorldAgent extends WorldEntity {
    public static final String healthPropertyName = "Health";
    public static final String livesPropertyName = "Lives";
    public static final String timePerStepMsPropertyName = "timePerStepMs";
    public static final String deathPropertyName = "Death";
    public static final String viewRangePropertyName = "ViewRange";
    private AgentAI ai = null;
    private int maxHealth = 0;
    private int currHealth = 0;
    private int deathTimeoutSec = 0;
    private int noLives = 0;
    private float timePerStepMs = 0.0F;
    private int viewRange = 0;
    private int reactionSpeed = 0;

    WorldAgent(float posX, float posY, float posZ, float oriX, float oriY, float oriZ, String identifier, Geometry g, AgentAI ai, int timePerStepMs, int initHealth, int noLives, int viewRange, int reactionSpeed, int deathTimeoutSec) {
        super(posX, posY, posZ, oriX, oriY, oriZ, true, true, identifier, g, timePerStepMs);
        this.ai = ai;
        this.maxHealth = initHealth;
        this.currHealth = initHealth;
        this.deathTimeoutSec = deathTimeoutSec;
        this.noLives = noLives;
        this.timePerStepMs = (float)timePerStepMs;
        this.viewRange = viewRange;
        this.reactionSpeed = reactionSpeed;
        this.addProperty(new WorldEntityProperty("timePerStepMs", Float.toString((float)timePerStepMs)));
        this.addProperty(new WorldEntityProperty("ViewRange", Integer.toString(viewRange)));
        this.addProperty(new WorldEntityProperty("Health", Integer.toString(this.currHealth)));
        this.addProperty(new WorldEntityProperty("Lives", Integer.toString(noLives)));
    }

    public int getCurrHealth() {
        return this.currHealth;
    }

    public int getMaxHealth() {
        return this.maxHealth;
    }

    public int getNoLives() {
        return this.noLives;
    }

    public float getTimePerStepMs() {
        return this.timePerStepMs;
    }

    public int getViewRange() {
        return this.viewRange;
    }

    public int getReactionSpeed() {
        return this.reactionSpeed;
    }

    public final int[] decideMove(ArrayList<int[]> moves, PacmanVisibleWorld mySurroundings) {
        StackTraceElement[] arr$;
        try {
            this.addProperty(new WorldEntityProperty("Health", Integer.toString(this.currHealth), 10));
            this.addProperty(new WorldEntityProperty("Lives", Integer.toString(this.noLives), 10));
            ArrayList e = new ArrayList();
            Iterator var10 = moves.iterator();

            int[] var12;
            while(var10.hasNext()) {
                var12 = (int[])var10.next();
                e.add(new int[]{var12[0], var12[1]});
            }

            int var11 = this.ai.decideMove(e, mySurroundings, this.getInfo());
            arr$ = null;
            if(var11 >= 0 && var11 < moves.size()) {
                var12 = (int[])moves.get(var11);
            } else {
                var12 = new int[]{0, 0};
            }

            this.applyTranslation((float)var12[0], (float)var12[1], 0.0F);
            return var12;
        } catch (Exception var9) {
            StringBuilder sb = new StringBuilder(var9.toString());
            sb.append(": ");
            sb.append(var9.getMessage());
            sb.append("StackTrace: ");
            arr$ = var9.getStackTrace();
            int len$ = arr$.length;

            for(int i$ = 0; i$ < len$; ++i$) {
                StackTraceElement el = arr$[i$];
                sb.append(el.toString() + " ");
            }

            MessageCollector.putError(this.ai.getClass().getSimpleName(), sb.toString());
            return new int[]{0, 0};
        }
    }

    public final void receiveDamage(int amount, WorldEntityProperty.WorldEntityTransformer deathTransformer) {
        this.currHealth -= amount;
        if(this.currHealth <= 0) {
            this.currHealth = this.maxHealth;
            --this.noLives;
            this.Reset();
            this.addProperty(new WorldEntityProperty("Lives", Integer.toString(this.noLives)));
            this.addProperty(new WorldEntityProperty("Death", "True", this.deathTimeoutSec * PacmanConstants.deathTimeoutMultiplier, deathTransformer, this));
        }

        this.addProperty(new WorldEntityProperty("Health", Integer.toString(this.currHealth)));
    }
}
