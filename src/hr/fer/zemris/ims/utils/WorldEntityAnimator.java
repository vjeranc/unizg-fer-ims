//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package hr.fer.zemris.ims.utils;

import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import java.io.IOException;

public class WorldEntityAnimator extends AbstractControl {
    Vector3f targetPosition = null;
    int timePerStepMs = 0;
    float timCorrection = 5.0F;

    public WorldEntityAnimator(Vector3f initPosition, int timePerStepMs) {
        this.targetPosition = initPosition;
        this.timePerStepMs = timePerStepMs;
    }

    protected void controlUpdate(float tpf) {
        Vector3f currPos = this.spatial.getLocalTranslation();
        float distance = currPos.distance(this.targetPosition);
        if(distance > 0.0F) {
            float part = (float)((int)(tpf * 1000.0F * this.timCorrection)) / (float)this.timePerStepMs;
            if(part > 1.0F) {
                part = 1.0F;
            }

            Vector3f difference = this.targetPosition.subtract(currPos);
            Vector3f shift = difference.mult(part);
            this.spatial.move(shift);
        }

    }

    public void moveTo(Vector3f newPosition) {
        this.targetPosition = newPosition;
    }

    protected void controlRender(RenderManager rm, ViewPort vp) {
    }

    public Control cloneForSpatial(Spatial spatial) {
        WorldEntityAnimator control = new WorldEntityAnimator(this.targetPosition, this.timePerStepMs);
        control.spatial = spatial;
        return control;
    }

    public void read(JmeImporter im) throws IOException {
        super.read(im);
        im.getCapsule(this);
    }

    public void write(JmeExporter ex) throws IOException {
        super.write(ex);
        ex.getCapsule(this);
    }
}
