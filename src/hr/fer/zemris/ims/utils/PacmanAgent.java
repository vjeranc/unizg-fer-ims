//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package hr.fer.zemris.ims.utils;

import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;

public class PacmanAgent extends WorldAgent {
    public static final String powerupPropertyName = "PowerUp";

    PacmanAgent(float posX, float posY, float posZ, float oriX, float oriY, float oriZ, Geometry g, AgentAI ai, int noLives, int viewRange, int timePerStepMs) {
        super(posX, posY, posZ, oriX, oriY, oriZ, "Pacman", g, ai, timePerStepMs, 1, noLives, viewRange, 1, 3);
    }

    public final void eatPowerup() {
        this.addProperty(new WorldEntityProperty("PowerUp", "True", PacmanConstants.powerupDuration, new WorldEntityProperty.WorldEntityTransformer() {
            public void performTransformation(Spatial geometry) {
                geometry.scale(2.0F);
            }

            public void undoTransformation(Spatial geometry) {
                geometry.scale(0.5F);
            }
        }, this));
    }

    public final void die() {
        this.receiveDamage(1, new WorldEntityProperty.WorldEntityTransformer() {
            public void performTransformation(Spatial geometry) {
                Material m = ((Geometry)geometry).getMaterial();
                m.setColor("Color", ColorRGBA.White);
            }

            public void undoTransformation(Spatial geometry) {
                Material m = ((Geometry)geometry).getMaterial();
                m.setColor("Color", ColorRGBA.Blue);
            }
        });
    }
}
