//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package hr.fer.zemris.ims.utils;

import com.jme3.scene.Geometry;

public class GhostAgent extends WorldAgent {
    GhostAgent(float posX, float posY, float posZ, float oriX, float oriY, float oriZ, Geometry g, AgentAI ai, int viewRange, int timePerStepMs, int ghostIndex) {
        super(posX, posY, posZ, oriX, oriY, oriZ, "Ghost", g, ai, timePerStepMs, 1, 2147483647, viewRange, 1 + ghostIndex, 3);
    }

    public final void die() {
        this.Reset();
    }
}
