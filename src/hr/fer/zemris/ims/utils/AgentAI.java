//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package hr.fer.zemris.ims.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

public class AgentAI {
    public AgentAI() {
    }

    public int decideMove(ArrayList<int[]> moves, PacmanVisibleWorld mySurroundings, WorldEntity.WorldEntityInfo myInfo) {
        Date now = new Date();
        Random r = new Random(now.getTime());
        int choice = r.nextInt(moves.size());
        return choice;
    }

    public final void printStatus(String s) {
        MessageCollector.putMessage(this.getClass().getSimpleName(), s);
    }
}
