//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package hr.fer.zemris.ims.utils;

import java.nio.charset.Charset;
import java.util.Locale;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

public class CompilerWrapper {
    JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();

    CompilerWrapper() {
        DiagnosticCollector fManDiagListener = new DiagnosticCollector();
        this.compiler.getStandardFileManager(fManDiagListener, (Locale)null, (Charset)null);
    }
}
