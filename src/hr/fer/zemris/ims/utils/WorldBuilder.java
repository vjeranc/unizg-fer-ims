//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package hr.fer.zemris.ims.utils;

import com.jme3.asset.AssetManager;
import java.util.ArrayList;
import java.util.HashMap;

public abstract class WorldBuilder {
    public WorldBuilder() {
    }

    public abstract HashMap<String, String> populateWold(ArrayList<WorldEntity>[][][] var1, int var2, HashMap<String, String> var3, AssetManager var4);
}
