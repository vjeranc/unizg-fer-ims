//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package hr.fer.zemris.ims.utils;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Sphere;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class PacmanBuilder extends WorldBuilder {
    public PacmanBuilder() {
    }

    public static void main(String... args) {
        System.out.println(neighbours.all.ordinal());
        System.out.println(neighbours.top.ordinal());
        System.out.println(neighbours.bottom.ordinal());
        System.out.println(neighbours.left.ordinal());
        System.out.println(neighbours.right.ordinal());
    }

    private void dec2DNeigh(int[][] array, int pos1, int pos2, PacmanBuilder.neighbours n, boolean check) {
        int[][] neighbours = (int[][])null;
        switch(n.ordinal()) {
        case 1:
            neighbours = new int[][]{{-1, 1}, {0, 1}, {1, 1}};
            break;
        case 2:
            neighbours = new int[][]{{1, -1}, {0, -1}, {-1, -1}};
            break;
        case 3:
            neighbours = new int[][]{{-1, -1}, {-1, 0}, {-1, 1}};
            break;
        case 4:
            neighbours = new int[][]{{1, 1}, {1, 0}, {1, -1}};
            break;
        default:
            neighbours = new int[][]{{0, 1}, {1, 1}, {1, 0}, {1, -1}, {0, -1}, {-1, -1}, {-1, 0}, {-1, 1}};
        }

        for(int i = 0; i < neighbours.length; ++i) {
            int dim1 = pos1 + neighbours[i][0];
            int dim2 = pos2 + neighbours[i][1];
            if(!check || dim1 > 0 && dim2 > 0 && dim1 < array.length && dim2 < array[0].length) {
                int val = array[dim1][dim2];
                array[dim1][dim2] = val > 0?val - 1:0;
            }
        }

    }

    public HashMap<String, String> populateWold(ArrayList<WorldEntity>[][][] world, int worldTimeStepMs, HashMap<String, String> builderParams, AssetManager assetManager) {
        Box b = new Box(0.5F, 0.5F, 0.5F);
        Sphere ss = new Sphere(18, 36, 0.2F);
        Sphere sl = new Sphere(18, 36, 0.3F);
        Material matBl = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        matBl.setColor("Color", ColorRGBA.Blue);
        Material matBr = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        matBr.setColor("Color", ColorRGBA.Brown);
        Material matRd = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        matRd.setColor("Color", ColorRGBA.Red);
        Material matGr = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        matGr.setColor("Color", ColorRGBA.Green);
        Material matYl = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        matYl.setColor("Color", ColorRGBA.Yellow);
        Geometry wallGeometry = new Geometry("Box", b);
        wallGeometry.setMaterial(matBr);
        Geometry powerupGeometry = new Geometry("SphereS", ss);
        powerupGeometry.setMaterial(matGr);
        Geometry pointGeometry = new Geometry("SphereS", ss);
        pointGeometry.setMaterial(matYl);
        Geometry pacmanGeometry = new Geometry("SphereL", sl);
        pacmanGeometry.setMaterial(matBl);
        Geometry ghostGeometry = new Geometry("SphereL", sl);
        ghostGeometry.setMaterial(matRd);
        int noGhosts = Integer.parseInt((String)builderParams.get("noGhosts"));
        int pitDimX = Integer.parseInt((String)builderParams.get("pitDimX"));
        int pitDimY = Integer.parseInt((String)builderParams.get("pitDimY"));
        String classPath = (String)builderParams.get("classPath");
        String[] path = classPath != null?new String[]{classPath}:new String[0];
        String pacmanAIName = (String)builderParams.get("pacmanAI");
        String ghostAIName = (String)builderParams.get("ghostAI");
        ArrayList aiNames = new ArrayList();
        aiNames.add(pacmanAIName);

        for(int ais = 0; ais < noGhosts; ++ais) {
            aiNames.add(ghostAIName);
        }

        List var50 = AIFactory.createAI(path, aiNames);
        int dimensionX = world.length;
        int dimensionY = world[0].length;
        float baseX = (float)(-dimensionX / 2) + 0.5F;
        float baseY = (float)(-dimensionY / 2) + 0.5F;
        float baseXBox = (float)(-pitDimX / 2) + 0.5F;
        float baseYBox = (float)(-pitDimY / 2) + 0.5F;
        int offsetBoxX = (dimensionX - pitDimX) / 2;
        int offsetBoxY = (dimensionY - pitDimY) / 2;
        int[][] helper = new int[dimensionX][dimensionY];

        int pw1;
        for(pw1 = 0; pw1 < dimensionX; ++pw1) {
            Arrays.fill(helper[pw1], 8);
        }

        WorldEntity pw2;
        WorldEntity pw3;
        for(pw1 = 0; pw1 < dimensionX; ++pw1) {
            pw2 = new WorldEntity(baseX + (float)pw1, baseY, 0.5F, 0.0F, 0.0F, 0.0F, false, false, "Wall", wallGeometry.clone(), worldTimeStepMs);
            world[pw1][0][0].add(pw2);
            helper[pw1][0] = 0;
            this.dec2DNeigh(helper, pw1, 0, PacmanBuilder.neighbours.top, true);
            pw3 = new WorldEntity(baseX + (float)pw1, baseY + (float)dimensionY - 1.0F, 0.5F, 0.0F, 0.0F, 0.0F, false, false, "Wall", wallGeometry.clone(), worldTimeStepMs);
            world[pw1][dimensionY - 1][0].add(pw3);
            helper[pw1][dimensionY - 1] = 0;
            this.dec2DNeigh(helper, pw1, dimensionY - 1, PacmanBuilder.neighbours.bottom, true);
        }

        for(pw1 = 1; pw1 < dimensionY - 1; ++pw1) {
            pw2 = new WorldEntity(baseX, baseY + (float)pw1, 0.5F, 0.0F, 0.0F, 0.0F, false, false, "Wall", wallGeometry.clone(), worldTimeStepMs);
            world[0][pw1][0].add(pw2);
            helper[0][pw1] = 0;
            this.dec2DNeigh(helper, 0, pw1, PacmanBuilder.neighbours.right, true);
            pw3 = new WorldEntity(baseX + (float)dimensionX - 1.0F, baseY + (float)pw1, 0.5F, 0.0F, 0.0F, 0.0F, false, false, "Wall", wallGeometry.clone(), worldTimeStepMs);
            world[dimensionX - 1][pw1][0].add(pw3);
            helper[dimensionX - 1][pw1] = 0;
            this.dec2DNeigh(helper, dimensionX - 1, pw1, PacmanBuilder.neighbours.left, true);
        }

        for(pw1 = 0; pw1 < pitDimX; ++pw1) {
            pw2 = new WorldEntity(baseXBox + (float)pw1, baseYBox, 0.5F, 0.0F, 0.0F, 0.0F, false, false, "Wall", wallGeometry.clone(), worldTimeStepMs);
            world[offsetBoxX + pw1][offsetBoxY + 0][0].add(pw2);
            helper[offsetBoxX + pw1][offsetBoxY + 0] = 0;
            this.dec2DNeigh(helper, offsetBoxX + pw1, offsetBoxY + 0, PacmanBuilder.neighbours.top, false);
            this.dec2DNeigh(helper, offsetBoxX + pw1, offsetBoxY + 0, PacmanBuilder.neighbours.bottom, false);
            pw3 = new WorldEntity(baseXBox + (float)pw1, baseYBox + (float)pitDimY - 1.0F, 0.5F, 0.0F, 0.0F, 0.0F, false, false, "Wall", wallGeometry.clone(), worldTimeStepMs);
            world[offsetBoxX + pw1][offsetBoxY + pitDimY - 1][0].add(pw3);
            helper[offsetBoxX + pw1][offsetBoxY + pitDimY - 1] = 0;
            this.dec2DNeigh(helper, offsetBoxX + pw1, offsetBoxY + pitDimY - 1, PacmanBuilder.neighbours.top, false);
            this.dec2DNeigh(helper, offsetBoxX + pw1, offsetBoxY + pitDimY - 1, PacmanBuilder.neighbours.bottom, false);
        }

        for(pw1 = 1; pw1 < pitDimY - 1; ++pw1) {
            pw2 = new WorldEntity(baseXBox, baseYBox + (float)pw1, 0.5F, 0.0F, 0.0F, 0.0F, false, false, "Wall", wallGeometry.clone(), worldTimeStepMs);
            world[offsetBoxX + 0][offsetBoxY + pw1][0].add(pw2);
            helper[offsetBoxX + 0][offsetBoxY + pw1] = 0;
            this.dec2DNeigh(helper, offsetBoxX + 0, offsetBoxY + pw1, PacmanBuilder.neighbours.left, false);
            this.dec2DNeigh(helper, offsetBoxX + 0, offsetBoxY + pw1, PacmanBuilder.neighbours.right, false);
            if(pw1 != pitDimY / 2) {
                pw3 = new WorldEntity(baseXBox + (float)pitDimX - 1.0F, baseYBox + (float)pw1, 0.5F, 0.0F, 0.0F, 0.0F, false, false, "Wall", wallGeometry.clone(), worldTimeStepMs);
                world[offsetBoxX + pitDimX - 1][offsetBoxY + pw1][0].add(pw3);
                helper[offsetBoxX + pitDimX - 1][offsetBoxY + pw1] = 0;
                this.dec2DNeigh(helper, offsetBoxX + pitDimX - 1, offsetBoxY + pw1, PacmanBuilder.neighbours.left, false);
                this.dec2DNeigh(helper, offsetBoxX + pitDimX - 1, offsetBoxY + pw1, PacmanBuilder.neighbours.right, false);
            }
        }

        WorldEntity var51 = new WorldEntity(baseX + 1.0F, baseY + 1.0F, 0.5F, 0.0F, 0.0F, 0.0F, false, false, "Powerup", powerupGeometry.clone(), worldTimeStepMs);
        world[1][1][0].add(var51);
        pw2 = new WorldEntity(baseX + (float)dimensionX - 2.0F, baseY + 1.0F, 0.5F, 0.0F, 0.0F, 0.0F, false, false, "Powerup", powerupGeometry.clone(), worldTimeStepMs);
        world[dimensionX - 2][1][0].add(pw2);
        pw3 = new WorldEntity(baseX + 1.0F, baseY + (float)dimensionY - 2.0F, 0.5F, 0.0F, 0.0F, 0.0F, false, false, "Powerup", powerupGeometry.clone(), worldTimeStepMs);
        world[1][dimensionY - 2][0].add(pw3);
        WorldEntity pw4 = new WorldEntity(baseX + (float)dimensionX - 2.0F, baseY + (float)dimensionY - 2.0F, 0.5F, 0.0F, 0.0F, 0.0F, false, false, "Powerup", powerupGeometry.clone(), worldTimeStepMs);
        world[dimensionX - 2][dimensionY - 2][0].add(pw4);
        Date now = new Date();
        Random r = new Random(now.getTime());
        int maxInnerWalls = dimensionX * dimensionY / 5;
        int countInnerWalls = 0;

        int noPoints;
        int noPlacedGhosts;
        for(int countAttempts = 0; countInnerWalls < maxInnerWalls && countAttempts < maxInnerWalls * 5; ++countAttempts) {
            noPoints = 1 + r.nextInt(dimensionX - 2);
            noPlacedGhosts = 1 + r.nextInt(dimensionY - 2);
            if(world[noPoints][noPlacedGhosts][0].isEmpty() && (noPoints < offsetBoxX || noPoints >= offsetBoxX + pitDimX || noPlacedGhosts < offsetBoxY || noPlacedGhosts >= offsetBoxY + pitDimY) && helper[noPoints][noPlacedGhosts] >= 7) {
                WorldEntity worldInfo = new WorldEntity(baseX + (float)noPoints, baseY + (float)noPlacedGhosts, 0.5F, 0.0F, 0.0F, 0.0F, false, false, "Wall", wallGeometry.clone(), worldTimeStepMs);
                world[noPoints][noPlacedGhosts][0].add(worldInfo);
                helper[noPoints][noPlacedGhosts] = 0;
                this.dec2DNeigh(helper, noPoints, noPlacedGhosts, PacmanBuilder.neighbours.all, false);
                ++countInnerWalls;
            }
        }

        noPoints = 0;

        int var52;
        for(noPlacedGhosts = 1; noPlacedGhosts < dimensionX - 1; ++noPlacedGhosts) {
            for(var52 = 1; var52 < dimensionY - 1; ++var52) {
                if(world[noPlacedGhosts][var52][0].isEmpty() && (noPlacedGhosts < offsetBoxX || noPlacedGhosts >= offsetBoxX + pitDimX || var52 < offsetBoxY || var52 >= offsetBoxY + pitDimY)) {
                    WorldEntity randomJ = new WorldEntity(baseX + (float)noPlacedGhosts, baseY + (float)var52, 0.5F, 0.0F, 0.0F, 0.0F, false, false, "Point", pointGeometry.clone(), worldTimeStepMs);
                    world[noPlacedGhosts][var52][0].add(randomJ);
                    ++noPoints;
                }
            }
        }

        noPlacedGhosts = 0;

        int var53;
        for(var52 = 1; var52 < pitDimX - 1; ++var52) {
            for(var53 = 1; var53 < pitDimY - 1; ++var53) {
                float var10002 = baseXBox + (float)var52;
                float var10003 = baseYBox + (float)var53;
                Geometry var10008 = ghostGeometry.clone();
                AgentAI var10009 = (AgentAI)var50.get(1 + noPlacedGhosts);
                ++noPlacedGhosts;
                GhostAgent pacman = new GhostAgent(var10002, var10003, 0.5F, 0.0F, 0.0F, 0.0F, var10008, var10009, 3, worldTimeStepMs, noPlacedGhosts);
                world[offsetBoxX + var52][offsetBoxX + var53][0].add(pacman);
                if(noPlacedGhosts >= noGhosts) {
                    break;
                }
            }

            if(noPlacedGhosts >= noGhosts) {
                break;
            }
        }

        do {
            do {
                var52 = 1 + r.nextInt(dimensionX - 2);
                var53 = 1 + r.nextInt(dimensionY - 2);
            } while(var52 >= offsetBoxX && var52 < offsetBoxX + pitDimX && var53 >= offsetBoxY && var53 < offsetBoxY + pitDimY);
        } while(((WorldEntity)world[var52][var53][0].get(0)).getIdentifier().compareTo("Wall") == 0);

        PacmanAgent var55 = new PacmanAgent(baseX + (float)var52, baseY + (float)var53, 0.5F, 0.0F, 0.0F, 0.0F, pacmanGeometry.clone(), (AgentAI)var50.get(0), PacmanConstants.numberOfLives, 3, worldTimeStepMs);
        world[var52][var53][0].add(var55);
        HashMap var54 = new HashMap();
        var54.put("noPoints", Integer.toString(noPoints + 4));
        return var54;
    }

    private static enum neighbours {
        all,
        top,
        bottom,
        left,
        right;

        private neighbours() {
        }
    }
}
