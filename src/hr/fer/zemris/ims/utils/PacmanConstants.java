package hr.fer.zemris.ims.utils;

/**
 * Created by vjeran on 21.06.15..
 */
public class PacmanConstants {
    public static final int speedup = 1;
    public static final int refreshRate = 500 / speedup;
    public static final int deathTimeoutMultiplier =refreshRate * 2;
    public static final int powerupDuration = refreshRate * 10;
    public static final int numberOfLives = 4;
    public static final String PACMAN_BUILDER = "hr.fer.zemris.ims.utils.PacmanBuilder";
    public static final String DEFAULT_GHOST_AI = "hr.fer.zemris.ims.lab.GhostAI";
    public static final String DEFAULT_PACMAN_AI = "hr.fer.zemris.ims.lab.PacmanAI";
}
