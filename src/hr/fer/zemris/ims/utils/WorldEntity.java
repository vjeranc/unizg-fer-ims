//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package hr.fer.zemris.ims.utils;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

public class WorldEntity {
    public static final String selectedPropertyName = "Selected";
    public static final String movablePropertyName = "Movable";
    public static final String intelligentPropertyName = "Intelligent";
    public static final String propertyValueTrue = "True";
    public static final String propertyValueFalse = "False";
    private static int idGen = 0;
    private int ID = 0;
    private HashMap<String, WorldEntityProperty> state = new HashMap();
    private Spatial sceneEl = null;
    private WorldEntityAnimator mover = null;
    private String identifier = null;
    private Vector3f posCopy = null;
    private Vector3f oriCopy = null;
    private Vector3f pos = null;
    private Vector3f ori = null;

    private static int generateID() {
        return idGen++;
    }

    public WorldEntity(float posX, float posY, float posZ, float oriX, float oriY, float oriZ, boolean movable, boolean intelligent, String identifier, Spatial sceneEl, int timePerStepMs) {
        this.ID = generateID();
        this.identifier = identifier;
        this.sceneEl = sceneEl;
        this.pos = new Vector3f(posX, posY, posZ);
        this.ori = new Vector3f(oriX, oriY, oriZ);
        this.sceneEl.move(this.pos);
        this.posCopy = new Vector3f(this.pos);
        this.oriCopy = new Vector3f(this.ori);
        this.mover = new WorldEntityAnimator(this.posCopy, idGen);
        this.sceneEl.addControl(this.mover);
        sceneEl.setLocalTranslation(this.pos);
        Quaternion q = new Quaternion();
        sceneEl.setLocalRotation(q.fromAngles(this.ori.x, this.ori.y, this.ori.z));
        this.addProperty(new WorldEntityProperty("Movable", movable?"True":"False"));
        this.addProperty(new WorldEntityProperty("Intelligent", intelligent?"True":"False"));
    }

    public final Vector3f getPos() {
        return new Vector3f(this.pos);
    }

    public final Vector3f getOri() {
        return new Vector3f(this.ori);
    }

    public final Spatial getSpatial() {
        return this.sceneEl;
    }

    public final String getIdentifier() {
        return this.identifier;
    }

    public final int getID() {
        return this.ID;
    }

    public final void checkProperties() {
        HashSet tempKeys = new HashSet(this.state.keySet());
        Iterator i$ = tempKeys.iterator();

        while(i$.hasNext()) {
            String propName = (String)i$.next();
            WorldEntityProperty prop = (WorldEntityProperty)this.state.get(propName);
            prop.checkProperty();
            if(prop.hasExpired()) {
                this.state.remove(propName);
            }
        }

    }

    public final void Reset() {
        this.sceneEl.setLocalTranslation(this.posCopy);
        Quaternion q = new Quaternion();
        this.sceneEl.setLocalRotation(q.fromAngles(this.oriCopy.x, this.oriCopy.y, this.oriCopy.z));
        this.pos.set(this.posCopy);
        this.ori.set(this.oriCopy);
    }

    public final void removeFromScene() {
        this.sceneEl.getParent().detachChild(this.sceneEl);
    }

    protected final void applyTranslation(float x, float y, float z) {
        Vector3f move = new Vector3f(x, y, z);
        this.pos = this.pos.add(move);
        this.mover.moveTo(this.pos);
    }

    public final WorldEntity.WorldEntityInfo getInfo() {
        HashMap exportState = new HashMap();
        Iterator newInfo = this.state.values().iterator();

        while(newInfo.hasNext()) {
            WorldEntityProperty prop = (WorldEntityProperty)newInfo.next();
            if(!prop.hasExpired()) {
                exportState.put(prop.getName(), prop.getValue());
            }
        }

        WorldEntity.WorldEntityInfo newInfo1 = new WorldEntity.WorldEntityInfo(this.identifier, this.ID, new Vector3f(this.getPos()), new Vector3f(this.getOri()), exportState);
        return newInfo1;
    }

    public final void addProperty(WorldEntityProperty p) {
        WorldEntityProperty oldP = (WorldEntityProperty)this.state.put(p.getName(), p);
        if(oldP != null) {
            oldP.deactivateProperty();
        }

    }

    public final WorldEntityProperty getProperty(String name) {
        return (WorldEntityProperty)this.state.get(name);
    }

    public final boolean hasProperty(String name) {
        return this.state.containsKey(name);
    }

    public static final class WorldEntityInfo {
        private String identifier;
        private int ID;
        private Vector3f position;
        private Vector3f orientation;
        private HashMap<String, String> state;

        public WorldEntityInfo(String identifier, int ID, Vector3f position, Vector3f orientation, HashMap<String, String> state) {
            this.position = position;
            this.orientation = orientation;
            this.state = state;
            this.identifier = identifier;
            this.ID = ID;
        }

        public Vector3f getPosition() {
            return this.position;
        }

        public Vector3f getOrientation() {
            return this.orientation;
        }

        public String getIdentifier() {
            return this.identifier;
        }

        public int getID() {
            return this.ID;
        }

        public String getProperty(String name) {
            return (String)this.state.get(name);
        }

        public boolean hasProperty(String name) {
            return this.state.containsKey(name);
        }
    }
}
