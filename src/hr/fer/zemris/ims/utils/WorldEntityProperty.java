//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package hr.fer.zemris.ims.utils;

import com.jme3.scene.Spatial;
import java.util.GregorianCalendar;

public final class WorldEntityProperty {
    private String name;
    private String value;
    private boolean limited;
    private boolean expired;
    private GregorianCalendar expirationTime;
    private WorldEntityProperty.WorldEntityTransformer transformer;
    private WorldEntity worldEntity;

    public WorldEntityProperty(String name, String value, int durationMs, WorldEntityProperty.WorldEntityTransformer tr, WorldEntity en) {
        this.expirationTime = new GregorianCalendar();
        this.expirationTime.add(14, durationMs);
        this.limited = durationMs > 0;
        this.expired = false;
        this.name = name;
        this.value = value;
        if(tr != null && en != null) {
            this.transformer = tr;
            this.worldEntity = en;
            tr.performTransformation(this.worldEntity.getSpatial());
        }

    }

    public WorldEntityProperty(String name, String value) {
        this(name, value, 0, (WorldEntityProperty.WorldEntityTransformer)null, (WorldEntity)null);
    }

    public WorldEntityProperty(String name, String value, int durationMs) {
        this(name, value, durationMs, (WorldEntityProperty.WorldEntityTransformer)null, (WorldEntity)null);
    }

    public WorldEntityProperty(String name, String value, WorldEntityProperty.WorldEntityTransformer tr, WorldEntity en) {
        this(name, value, 0, tr, en);
    }

    public String getName() {
        return this.name;
    }

    public String getValue() {
        return this.value;
    }

    public boolean isLimited() {
        return this.limited;
    }

    public final boolean hasExpired() {
        return this.expired;
    }

    public final void deactivateProperty() {
        if(this.transformer != null && this.worldEntity != null) {
            this.transformer.undoTransformation(this.worldEntity.getSpatial());
        }

        this.expired = true;
    }

    public final void checkProperty() {
        if(this.limited && !this.expired) {
            GregorianCalendar now = new GregorianCalendar();
            if(now.compareTo(this.expirationTime) >= 0) {
                this.deactivateProperty();
            }
        }

    }

    public interface WorldEntityTransformer {
        void performTransformation(Spatial var1);

        void undoTransformation(Spatial var1);
    }
}
