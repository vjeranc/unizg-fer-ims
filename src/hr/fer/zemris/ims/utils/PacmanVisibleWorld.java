//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package hr.fer.zemris.ims.utils;

import java.util.ArrayList;
import java.util.HashMap;

public final class PacmanVisibleWorld {
    private int gridDimensionX;
    private int gridDimensionY;
    private ArrayList<WorldEntity.WorldEntityInfo>[][] visibleWorld;
    private HashMap<Integer, Object>[][] metadata;
    private boolean[][] editableMetadata;

    PacmanVisibleWorld(ArrayList<WorldEntity.WorldEntityInfo>[][] visibleWorld, HashMap<Integer, Object>[][] metadata, boolean[][] editableMetadata) {
        this.gridDimensionX = visibleWorld.length;
        this.gridDimensionY = visibleWorld[0].length;
        this.visibleWorld = visibleWorld;
        this.metadata = metadata;
        this.editableMetadata = editableMetadata;
    }

    public final int getDimensionX() {
        return this.gridDimensionX;
    }

    public final int getDimensionY() {
        return this.gridDimensionY;
    }

    public final ArrayList<WorldEntity.WorldEntityInfo> getWorldInfoAt(float x, float y) {
        return this.editableMetadata[(int)x + this.gridDimensionX / 2][(int)y + this.gridDimensionY / 2]?this.visibleWorld[(int)x + this.gridDimensionX / 2][(int)y + this.gridDimensionY / 2]:null;
    }

    public final HashMap<Integer, Object> getWorldMetadataAt(float x, float y) {
        return this.editableMetadata[(int)x + this.gridDimensionX / 2][(int)y + this.gridDimensionY / 2]?this.metadata[(int)x + this.gridDimensionX / 2][(int)y + this.gridDimensionY / 2]:null;
    }
}
