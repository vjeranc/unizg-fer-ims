//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package hr.fer.zemris.ims.utils;

import com.jme3.asset.AssetManager;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

public class WorldFactory {
    public WorldFactory() {
    }

    public static PacmanWorld createWorld(String worldBuilderName, int dimensionX, int dimensionY, int dimensionZ, int worldTimeStepMs, HashMap<String, String> builderParams, AssetManager assetManager) {
        try {
            Class ex = WorldFactory.class.getClassLoader().loadClass(worldBuilderName);
            if(WorldBuilder.class.isAssignableFrom(ex)) {
                Constructor con = ex.getConstructor(new Class[0]);
                WorldBuilder wb = (WorldBuilder)con.newInstance(new Object[0]);
                PacmanWorld world = new PacmanWorld(dimensionX, dimensionY, dimensionZ, worldTimeStepMs, wb, builderParams, assetManager);
                return world;
            } else {
                MessageCollector.putError("WorldFactory", worldBuilderName + " is not a subclass of WorldBuilder!");
                return null;
            }
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException | ClassNotFoundException var11) {
            MessageCollector.putError("WorldFactory", var11.getMessage());
            return null;
        }
    }
}
