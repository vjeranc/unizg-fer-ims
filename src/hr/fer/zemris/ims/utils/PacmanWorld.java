//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package hr.fer.zemris.ims.utils;

import com.jme3.asset.AssetManager;
import com.jme3.math.Vector3f;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeMap;

public class PacmanWorld {
    private int gridDimensionX = 0;
    private int gridDimensionY = 0;
    private int gridDimensionZ = 0;
    private float gridStepX = 1.0F;
    private float gridStepY = 1.0F;
    private float gridStepZ = 1.0F;
    int worldTimeStepMs = 0;
    private ArrayList<WorldEntity>[][][] world = (ArrayList[][][])null;
    private HashMap<Integer, Object>[][][] metadata = (HashMap[][][])null;
    private HashSet<WorldEntity> worldElementList = new HashSet();
    private HashMap<Integer, WorldAgent> worldAgentIDList = new HashMap();
    private TreeMap<Integer, WorldAgent> worldAgentReactionList = new TreeMap();
    private int noTotalPoints = 0;
    private int noPointsLeft = 0;

    PacmanWorld(int dimensionX, int dimensionY, int dimensionZ, int worldTimeStepMs, WorldBuilder wb, HashMap<String, String> builderParams, AssetManager assetManager) {
        this.gridDimensionX = dimensionX;
        this.gridDimensionY = dimensionY;
        this.gridDimensionZ = dimensionZ;
        this.worldTimeStepMs = worldTimeStepMs;
        this.world = (ArrayList[][][])((ArrayList[][][])Array.newInstance(ArrayList.class, new int[]{dimensionX, dimensionY, dimensionZ}));
        this.metadata = (HashMap[][][])((HashMap[][][])Array.newInstance(HashMap.class, new int[]{dimensionX, dimensionY, dimensionZ}));

        int i;
        int j;
        for(int worldInfo = 0; worldInfo < dimensionX; ++worldInfo) {
            for(i = 0; i < dimensionY; ++i) {
                for(j = 0; j < dimensionZ; ++j) {
                    this.world[worldInfo][i][j] = new ArrayList();
                    this.metadata[worldInfo][i][j] = new HashMap();
                }
            }
        }

        HashMap var16 = wb.populateWold(this.world, worldTimeStepMs, builderParams, assetManager);
        this.noTotalPoints = Integer.parseInt((String)var16.get("noPoints"));
        this.noPointsLeft = this.noTotalPoints;

        for(i = 0; i < dimensionX; ++i) {
            for(j = 0; j < dimensionY; ++j) {
                for(int k = 0; k < dimensionZ; ++k) {
                    ArrayList currEntities = this.world[i][j][k];
                    this.worldElementList.addAll(currEntities);
                    Iterator i$ = currEntities.iterator();

                    while(i$.hasNext()) {
                        WorldEntity en = (WorldEntity)i$.next();
                        if(en instanceof WorldAgent) {
                            WorldAgent ag = (WorldAgent)en;
                            this.worldAgentIDList.put(Integer.valueOf(ag.getID()), ag);
                            this.worldAgentReactionList.put(Integer.valueOf(ag.getReactionSpeed()), ag);
                        }
                    }
                }
            }
        }

    }

    public final int getDimensionX() {
        return this.gridDimensionX;
    }

    public final int getDimensionY() {
        return this.gridDimensionY;
    }

    public final int getDimensionZ() {
        return this.gridDimensionZ;
    }

    public int getWorldTimeStepMs() {
        return this.worldTimeStepMs;
    }

    public final int getNoTotalPoints() {
        return this.noTotalPoints;
    }

    public final int getPointsEaten() {
        return this.noTotalPoints - this.noPointsLeft;
    }

    public final HashSet<WorldEntity> getEntities() {
        return this.worldElementList;
    }

    public final PacmanWorld.Outcome processAgents() {
        Iterator i$ = this.worldElementList.iterator();

        while(i$.hasNext()) {
            WorldEntity reaction = (WorldEntity)i$.next();
            reaction.checkProperties();
        }

        i$ = this.worldAgentReactionList.keySet().iterator();

        while(i$.hasNext()) {
            Integer var26 = (Integer)i$.next();
            WorldAgent ag = (WorldAgent)this.worldAgentReactionList.get(var26);
            Vector3f pos = ag.getPos();
            int locX = Math.round((pos.x - 0.5F) / this.gridStepX + (float)(this.gridDimensionX / 2));
            int locY = Math.round((pos.y - 0.5F) / this.gridStepY + (float)(this.gridDimensionY / 2));
            ArrayList entities = this.world[locX][locY][0];

            int list;
            int checkedList;
            for(int moves = 0; moves < entities.size(); ++moves) {
                WorldEntity neighbours = (WorldEntity)entities.get(moves);
                if(neighbours.getIdentifier().compareTo("Point") == 0 && ag.getIdentifier().compareTo("Pacman") == 0) {
                    --this.noPointsLeft;
                    entities.remove(neighbours);
                    neighbours.removeFromScene();
                    this.worldElementList.remove(neighbours);
                }

                if(neighbours.getIdentifier().compareTo("Powerup") == 0 && ag.getIdentifier().compareTo("Pacman") == 0) {
                    --this.noPointsLeft;
                    entities.remove(neighbours);
                    neighbours.removeFromScene();
                    this.worldElementList.remove(neighbours);
                    ((PacmanAgent)ag).eatPowerup();
                }

                if(neighbours.getIdentifier().compareTo("Ghost") == 0 && ag.getIdentifier().compareTo("Pacman") == 0 || ag.getIdentifier().compareTo("Ghost") == 0 && neighbours.getIdentifier().compareTo("Pacman") == 0) {
                    PacmanAgent mySurroundings = (PacmanAgent)((PacmanAgent)(ag.getIdentifier().compareTo("Pacman") == 0?ag:neighbours));
                    GhostAgent myMetadata = (GhostAgent)((GhostAgent)(ag.getIdentifier().compareTo("Ghost") == 0?ag:neighbours));
                    Vector3f myEditableMetadata;
                    if(mySurroundings.hasProperty("PowerUp")) {
                        myMetadata.die();
                        myEditableMetadata = myMetadata.getPos();
                        list = Math.round((myEditableMetadata.x - 0.5F) / this.gridStepX + (float)(this.gridDimensionX / 2));
                        checkedList = Math.round((myEditableMetadata.y - 0.5F) / this.gridStepY + (float)(this.gridDimensionY / 2));
                        entities.remove(myMetadata);
                        this.world[list][checkedList][0].add(myMetadata);
                    } else {
                        mySurroundings.die();
                        MessageCollector.putMessage("PacmanWorld", "Pacman has " + mySurroundings.getNoLives() + " lives left.");
                        myEditableMetadata = mySurroundings.getPos();
                        list = Math.round((myEditableMetadata.x - 0.5F) / this.gridStepX + (float)(this.gridDimensionX / 2));
                        checkedList = Math.round((myEditableMetadata.y - 0.5F) / this.gridStepY + (float)(this.gridDimensionY / 2));
                        entities.remove(mySurroundings);
                        this.world[list][checkedList][0].add(mySurroundings);
                        if(mySurroundings.getNoLives() <= 0) {
                            return PacmanWorld.Outcome.Defeat;
                        }
                    }
                }
            }

            pos = ag.getPos();
            locX = Math.round((pos.x - 0.5F) / this.gridStepX + (float)(this.gridDimensionX / 2));
            locY = Math.round((pos.y - 0.5F) / this.gridStepY + (float)(this.gridDimensionY / 2));
            ArrayList var27 = new ArrayList();
            int[][] var28 = new int[][]{{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
            if(ag.getProperty("Death") == null) {
                for(int var29 = 0; var29 < var28.length; ++var29) {
                    int var31 = locX + var28[var29][0];
                    int var33 = locY + var28[var29][1];
                    ArrayList var35 = this.world[var31][var33][0];
                    boolean var36 = false;
                    Iterator index = var35.iterator();

                    while(index.hasNext()) {
                        WorldEntity pvw = (WorldEntity)index.next();
                        if(pvw.getIdentifier().compareTo("Wall") == 0 || ag.getIdentifier().compareToIgnoreCase("Ghost") == 0 && pvw.getIdentifier().compareTo("Ghost") == 0) {
                            var36 = true;
                            break;
                        }
                    }

                    if(!var36) {
                        var27.add(var28[var29]);
                    }
                }
            }

            if(var27.isEmpty()) {
                var27.add(new int[]{0, 0});
            }

            ArrayList[][] var30 = (ArrayList[][])((ArrayList[][])Array.newInstance(ArrayList.class, new int[]{ag.getViewRange() * 2 + 1, ag.getViewRange() * 2 + 1}));
            HashMap[][] var32 = (HashMap[][])((HashMap[][])Array.newInstance(HashMap.class, new int[]{ag.getViewRange() * 2 + 1, ag.getViewRange() * 2 + 1}));
            boolean[][] var34 = new boolean[ag.getViewRange() * 2 + 1][ag.getViewRange() * 2 + 1];

            for(list = 0; list < ag.getViewRange() * 2 + 1; ++list) {
                for(checkedList = 0; checkedList < ag.getViewRange() * 2 + 1; ++checkedList) {
                    var30[list][checkedList] = new ArrayList();
                    var32[list][checkedList] = new HashMap();
                    var34[list][checkedList] = false;
                }
            }

            LinkedList var37 = new LinkedList();
            HashSet var39 = new HashSet();
            var37.add(new int[]{locX, locY});
            int var38 = locX * this.gridDimensionY * this.gridDimensionZ + locY * this.gridDimensionZ;
            var39.add(Integer.valueOf(var38));

            Iterator i$1;
            Integer id;
            WorldAgent fellowAg;
            HashMap var46;
            HashSet var47;
            while(!var37.isEmpty()) {
                int[] var40 = (int[])var37.removeLast();
                int move = Math.abs(var40[0] - locX) + Math.abs(var40[1] - locY);
                boolean i = false;
                ArrayList en = this.world[var40[0]][var40[1]][0];

                WorldEntity metakeys;
                for(Iterator meta = en.iterator(); meta.hasNext(); var30[var40[0] - locX + ag.getViewRange()][var40[1] - locY + ag.getViewRange()].add(metakeys.getInfo())) {
                    metakeys = (WorldEntity)meta.next();
                    if(metakeys.getIdentifier().compareTo("Wall") == 0) {
                        i = true;
                    }
                }

                var46 = this.metadata[var40[0]][var40[1]][0];
                var47 = new HashSet(var46.keySet());
                i$1 = var47.iterator();

                while(i$1.hasNext()) {
                    id = (Integer)i$1.next();
                    fellowAg = (WorldAgent)this.worldAgentIDList.get(id);
                    if(fellowAg != null && fellowAg.getIdentifier().compareToIgnoreCase(ag.getIdentifier()) == 0) {
                        Object newIndex = var46.remove(id);
                        var32[var40[0] - locX + ag.getViewRange()][var40[1] - locY + ag.getViewRange()].put(id, newIndex);
                    }
                }

                var34[var40[0] - locX + ag.getViewRange()][var40[1] - locY + ag.getViewRange()] = true;
                if(move < ag.getViewRange() && !i) {
                    for(int var48 = 0; var48 < var28.length; ++var48) {
                        int[] var49 = new int[]{var40[0] + var28[var48][0], var40[1] + var28[var48][1]};
                        int var50 = Math.abs(var49[0] - locX) + Math.abs(var49[1] - locY);
                        int var51 = var49[0] * this.gridDimensionY * this.gridDimensionZ + var49[1] * this.gridDimensionZ;
                        if(var39.add(Integer.valueOf(var51)) && var50 > move && var49[0] >= 0 && var49[1] >= 0 && var49[0] < this.gridDimensionX && var49[1] < this.gridDimensionY) {
                            var37.addFirst(var49);
                        }
                    }
                }
            }

            PacmanVisibleWorld var41 = new PacmanVisibleWorld(var30, var32, var34);
            int[] var42 = ag.decideMove(var27, var41);

            int var43;
            for(var43 = -var41.getDimensionX() / 2; var43 <= var41.getDimensionX() / 2; ++var43) {
                for(int var44 = -var41.getDimensionY() / 2; var44 <= var41.getDimensionY() / 2; ++var44) {
                    var46 = var41.getWorldMetadataAt((float)var43, (float)var44);
                    if(var46 != null && !var46.isEmpty()) {
                        var47 = new HashSet(var46.keySet());
                        i$1 = var47.iterator();

                        while(i$1.hasNext()) {
                            id = (Integer)i$1.next();
                            fellowAg = (WorldAgent)this.worldAgentIDList.get(id);
                            if(fellowAg != null && fellowAg.getIdentifier().compareToIgnoreCase(ag.getIdentifier()) == 0) {
                                this.metadata[locX + var43][locY + var44][0].put(id, var46.get(id));
                            }
                        }
                    }
                }
            }

            entities = this.world[locX][locY][0];

            for(var43 = 0; var43 < entities.size(); ++var43) {
                WorldEntity var45 = (WorldEntity)entities.get(var43);
                if(var45.getID() == ag.getID()) {
                    entities.remove(var43);
                    this.world[locX + var42[0]][locY + var42[1]][0].add(var45);
                    break;
                }
            }
        }

        if(this.noPointsLeft == 0) {
            return PacmanWorld.Outcome.Win;
        } else {
            return PacmanWorld.Outcome.Ongoing;
        }
    }

    public static enum Outcome {
        Win,
        Defeat,
        Tie,
        Ongoing;

        private Outcome() {
        }
    }
}
