//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package hr.fer.zemris.ims.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class AIFactory {
    public AIFactory() {
    }

    public static List<AgentAI> createAI(String[] classPaths, List<String> AINames) {
        ClassLoader parent = AIFactory.class.getClassLoader();
        AIClassLoader customCL = new AIClassLoader(classPaths, parent);
        ArrayList ais = new ArrayList();

        for(int i = 0; i < AINames.size(); ++i) {
            try {
                Class ex = customCL.loadClass((String)AINames.get(i));
                if(AgentAI.class.isAssignableFrom(ex)) {
                    Constructor con = ex.getConstructor(new Class[0]);
                    MessageCollector.putMessage("AI Factory", "Loading AI class: " + (String) AINames.get(i));
                    ais.add((AgentAI)con.newInstance(new Object[0]));
                } else {
                    MessageCollector.putError("AI Factory", (String) AINames.get(i) + " is not a subclass of AgentAI! Using default random AI");
                    ais.add(new AgentAI());
                }
            } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | ClassNotFoundException var8) {
                MessageCollector.putError("AIFactory", var8.toString() + ": " + var8.getMessage());
                MessageCollector.putMessage("AI Factory", "Using default random AI instead of " + (String) AINames.get(i));
                ais.add(new AgentAI());
            }
        }

        return ais;
    }
}
