//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package hr.fer.zemris.ims.utils;

import java.util.ArrayList;

public class MessageCollector {
    private static ArrayList<String> messages = new ArrayList();
    private static int i = 0;

    public MessageCollector() {
    }

    public static void putError(String author, String error) {
        messages.add("Exception " + i + ": " + author + " reports " + error);
        i = ++i > 0?i:0;
    }

    public static void putMessage(String author, String message) {
        messages.add("Message " + i + ": " + author + " says " + message);
        i = ++i > 0?i:0;
    }

    public static ArrayList<String> getMessages() {
        ArrayList tempMessages = new ArrayList(messages);
        messages.clear();
        return tempMessages;
    }
}
