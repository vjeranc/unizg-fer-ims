package hr.fer.zemris.ims.lab;

import hr.fer.zemris.ims.utils.AgentAI;
import hr.fer.zemris.ims.utils.PacmanAgent;
import hr.fer.zemris.ims.utils.PacmanVisibleWorld;
import hr.fer.zemris.ims.utils.WorldEntity;

import java.io.*;
import java.nio.file.*;
import java.util.*;

/**
 * Created by vjeran on 20.06.15..
 */
public class QPacmanAI extends AgentAI {
    private static final Path tablePath = Paths.get("./pacmanqtable.javaobject");
    /**
     * Q(s, a) table. Integer will represent a (8*3 + 1) bit encoded state of the surroundings.
     * There are four possible moves. Up, down, left, right, all will be encoded as indices in double[]
     * array where Q(s, a) values will be kept.
     */
    private static Map<Integer, double[]> Q = new HashMap<>();

    /**
     * Learning rate of the q-learning process.
     */
    private static final double alpha = 0.05;
    /**
     * Long-term / short-term award, the closer parameter is to 1, more is the long-term award favoured.
     */
    private static final double gamma = 0.6;
    /**
     * What's the probability of picking a random move instead of a best one.
     */
    private static final double epsilon = 0.3;

    private static final int[][] allNeighbors = {{0, 1}, {1, 1}, {1, 0}, {1, -1}, {0, -1}, {-1, -1}, {-1, 0}, {-1, 1}};
    private static final int[][] allMoves = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};

    private static final double pointAward = 5.0;
    private static final double powerupAward = 10.0;
    private static final double ghostEatenAward = 100.0;
    private static final double deathAward = 0.0;

    /** This is a permanent map to action. Action is just an index of Q(s) double[] array. */
    private static final int[][] moveToAction = new int[3][3];

    static {
        int cnt = 0;
                for(int[] move : allMoves) {
                    moveToAction[move[0]+1][move[1]+1] = cnt;
                    cnt++;
                }
        Q = loadQTable();
    }

    /** This is an action to award calculated each time so we can now how was the executed action awarded.
     *  There are 3 awards, for eating point, for eating a powerup and for eatiing a ghost.
     */
    private final double[] actionToAward = new double[allMoves.length];

    private int prevState = -1;
    private int prevAction = -1;

    private boolean initialized = false;


    /**
     * State is encoded in the following way.
     * There are 6 tile states:
     * Ghost, Powerup, Point, Wall, GhostPowerup, GhostPoint
     * There is 1 Pacman state:
     * Powerup effect
     *
     * In binary:
     * Ghost - 111
     * Powerup - 001
     * Point - 010
     * Wall - 011
     * GhostPowerup - 110
     * GhostPoint - 101
     *
     * If state == Ghost == 111
     * If property is powerup or point
     * state = state ^ (001 or 010) and we get the GhostPowerup or GhostPoint.
     *
     * One needs 8*3 + 1 bits of state for the current neighborhood of 8 visible fields.
     * @param world
     * @return
     */
    int encodeState(PacmanVisibleWorld world, WorldEntity.WorldEntityInfo pacmanInfo) {
        int state = 0; // 0000
        for(int[] neighbor : allNeighbors) {
            int x = neighbor[0],
                y = neighbor[1];
            state = state << 3; // clean for the new three bits
            List<WorldEntity.WorldEntityInfo> infos = world.getWorldInfoAt(x, y);
            if (infos == null) {
                continue;
            }
            for (WorldEntity.WorldEntityInfo info : infos) {
                String id = info.getIdentifier();
                // starting state for the new neighbor is always 0
                switch(id.toLowerCase()) {
                    case "ghost":
                        state = state | 0b0111; break; // 0111
                    case "powerup":
                        state = state ^ 0b0001; break; // 0001
                    case "point":
                        state = state ^ 0b0010; break; // 0010
                    case "wall":
                        state = state | 0b0011; break; // 0011
                    default:
                        throw new IllegalArgumentException("There's no identifer named : " + id);
                }
            }
        }

        // now we have all 8 tiles written in 24 bits
        // we have to write the pacman information, is he under the influence
        // of powerup or he's normal
        state = state << 1;
        if (pacmanInfo.hasProperty(PacmanAgent.powerupPropertyName)) {
            state = state | 0b01;
        }

        return state;
    }

    private void calcAwards(ArrayList<int[]> moves, PacmanVisibleWorld world, WorldEntity.WorldEntityInfo pacmanInfo) {
        boolean powerup = pacmanInfo.hasProperty(PacmanAgent.powerupPropertyName);
        for(int[] move : moves) {
            int x = move[0],
                y = move[1];
            double award = 0.005; // this will be the award if nothing is eaten.
            List<WorldEntity.WorldEntityInfo> infos = world.getWorldInfoAt(x, y);
            boolean ghostSeen = false; // if ghost is seen and no power up there shouldn't be an award
            for (WorldEntity.WorldEntityInfo info : infos) {
                String id = info.getIdentifier();
                // starting state for the new neighbor is always 0
                switch (id.toLowerCase()) {
                    case "ghost":
                        if (powerup) {
                            award += ghostEatenAward;
                        }
                        ghostSeen = true;
                        break; // 0111
                    case "powerup":
                        award += powerupAward;
                        break; // 0001
                    case "point":
                        award += pointAward;
                        break; // 0010
                }
            }
            if (!powerup && ghostSeen) {
                award = deathAward; // if this action is taken pacman will die
            }
            actionToAward[moveToActionTransform(move)] = award;
        }
    }

    private int moveToActionTransform(int[] move) {
        return moveToAction[move[0]+1][move[1]+1];
    }

    public int decideMove(ArrayList<int[]> moves, PacmanVisibleWorld mySurroundings, WorldEntity.WorldEntityInfo myInfo) {
        int state = encodeState(mySurroundings, myInfo);

        // pretpostavljam da je xspan == yspan == 3 - shodno tome allNeighbors
        // inače treba allNeighbors računati
        int xspan = mySurroundings.getDimensionX();
        int yspan = mySurroundings.getDimensionY();
        assert xspan == 3;
        assert yspan == 3;

        if (prevState != -1) {
            double qsa = getQState(prevState)[prevAction];
            double r = actionToAward[prevAction];
            double maxActionQ = getMaxActionQCost(state);
            Q.get(prevState)[prevAction] = qsa + alpha * (r + gamma * maxActionQ - qsa);
        }
        boolean wasReset = myInfo.hasProperty("Death");
        calcAwards(moves, mySurroundings, myInfo);
        int decidedMoveIndex = pickMoveUsingQ(state, moves);
        prevState = state;
        if (wasReset) {
            prevState = -1;
        }
        serializeQTable();
        return decidedMoveIndex;
    }

    private double getMaxActionQCost(int state) {
        double[] costs = getQState(state);
        double maxQCost = Double.MIN_VALUE;
        for(double cost : costs) {
            maxQCost = Math.max(cost, maxQCost);
        }
        return maxQCost;
    }

    private double[] getQState(int state) {
        double[] costs = Q.get(state);
        if (costs == null) {
            costs = new double[allMoves.length];
            for(int i = 0; i < costs.length; i++) {
                costs[i] = rand.nextDouble();
            }
            Q.put(state, costs);
        }
        return costs;
    }

    private Random rand = new Random();

    private int pickMoveUsingQ(int s, ArrayList<int[]> moves) {
        double[] costs = getQState(s);
        // do a move at random - 5% chance of happening
        if (rand.nextDouble() < epsilon) {
            // this maps a move [-1, 0] and similar to
            int randomIndex = rand.nextInt(moves.size());
            prevAction = moveToActionTransform(moves.get(randomIndex));
            // this needs to be set for Q(s, a) update after the move is made
            return randomIndex;
        }

        // if not random move then best move

        double maxCost = Double.MIN_VALUE;
        int maxActionRealIndex = 0;
        for(int i = 0; i < moves.size(); i++) {
            int[] move = moves.get(i);
            int action = moveToActionTransform(move);
            if (costs[action] > maxCost) {
                maxCost = costs[action];
                maxActionRealIndex = i;
                prevAction = action; // this needs to be set for Q(s, a) update after the move is made
            }
        }
        return maxActionRealIndex;
    }

    public void serializeQTable() {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out = null;
        try {
            out = new ObjectOutputStream(bos);
            out.writeObject(Q);
            byte[] yourBytes = bos.toByteArray();
            Files.write(tablePath, yourBytes, StandardOpenOption.CREATE);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException ex) {
                // ignore close exception
            }
            try {
                bos.close();
            } catch (IOException ex) {
                // ignore close exception
            }
        }
    }

    private static HashMap<Integer,double[]> loadQTable() {
        // load Q table
        if (!Files.exists(tablePath)) {
            return null;
        }
        ByteArrayInputStream bis = null;
        try {
            bis = new ByteArrayInputStream(Files.readAllBytes(tablePath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        ObjectInput in = null;
        try {
            in = new ObjectInputStream(bis);
            Object o = in.readObject();
            return (HashMap<Integer,double[]>) o;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                bis.close();
            } catch (IOException ex) {
                // ignore close exception
            }
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                // ignore close exception
            }
        }
        return null;
    }

}
