package hr.fer.zemris.ims.lab;

import hr.fer.zemris.ims.utils.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

/**
 * Created by vujevic on 21.06.15..
 */
public class QGhostAI extends AgentAI{
    private class Coordinate {
        public int x;
        public int y;

        public Coordinate(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof Coordinate){
                Coordinate koord = (Coordinate)o;
                return this.x == koord.x && this.y == koord.y;
            }else{
                return false;
            }
        }
    }
    private enum Identifikator {
        ZID, KOLAC, POWERUP, PRAZNO
    }
    private class Lokacija {
        Coordinate koord = null;
        private boolean zid = false;
        private boolean kolac = false;
        private boolean powerup = false;

        Lokacija(int x, int y, Identifikator identifikator){
            this.koord = new Coordinate(x, y);
            if (identifikator == Identifikator.ZID){
                this.zid = true;
            }else if (identifikator == Identifikator.KOLAC){
                this.kolac = true;
            }else if (identifikator == Identifikator.POWERUP){
                this.powerup = true;
            }
        }

        int getX() {return koord.x;}
        int getY() {return koord.y;}
        boolean isZid() {return this.zid;}
        boolean isKolac() {return this.kolac;}
        boolean isPowerup() {return this.powerup;}
    }

    private static final int mapaMaxX = 100;
    private static final int mapaMaxY = 100;
    private Lokacija[][] mapa = new Lokacija[mapaMaxX][mapaMaxY];
    private Coordinate mojaLokacija = null;
    private int pocetnaX = mapaMaxX/2;
    private int pocetnaY = mapaMaxY/2;
    private Random random = new Random(new Date().getTime());
    Coordinate odredisteKoord = null;

    public void GhostAI() {
        novaMapa();
    }

    private void novaMapa(){
        mojaLokacija = null;
        for (int i=0; i<mapaMaxX; i++){
            for (int j=0; j<mapaMaxY; j++){
                mapa[i][j] = null;
            }
        }
    }


    private Coordinate updateMapAndPacman(PacmanVisibleWorld okolina){
        Coordinate pacman = null;
        if (mojaLokacija == null){
            mojaLokacija = new Coordinate(pocetnaX, pocetnaY);
        }
        for(int i=-okolina.getDimensionX()/2; i<=okolina.getDimensionX()/2; i++){
            for(int j=-okolina.getDimensionY()/2; j<=okolina.getDimensionY()/2; j++){
                ArrayList<WorldEntity.WorldEntityInfo> lokacijaInfo = okolina.getWorldInfoAt(i, j);
                if (lokacijaInfo != null){
                    int lokacijaX = mojaLokacija.x + i;
                    int lokacijaY = mojaLokacija.y + j;
                    Identifikator identifikator = Identifikator.PRAZNO;
                    for(WorldEntity.WorldEntityInfo info : lokacijaInfo){
                        if (info.getIdentifier().compareToIgnoreCase("Wall") == 0){
                            identifikator = Identifikator.ZID;
                        }else if (info.getIdentifier().compareToIgnoreCase("Point") == 0){
                            identifikator = Identifikator.KOLAC;
                        }else if (info.getIdentifier().compareToIgnoreCase("Powerup") == 0){
                            identifikator = Identifikator.POWERUP;
                        }else if (info.getIdentifier().compareToIgnoreCase("Pacman") == 0){
                            pacman = new Coordinate(lokacijaX, lokacijaY);
                        }
                    }
                    mapa[lokacijaX][lokacijaY] = new Lokacija(lokacijaX, lokacijaY, identifikator);
                }
            }
        }
        return pacman;
    }

    private int idiPrema(Coordinate pacman, List<int[]> moves){
        int[] najblizi = null;
        int[][] udaljenostMapa = new int[mapaMaxX][mapaMaxY];
        int[][][] pomakMapa = new int[mapaMaxX][mapaMaxY][2];
        for (int i=0;i<mapaMaxX;i++){
            for (int j=0;j<mapaMaxY;j++){
                udaljenostMapa[i][j] = -1;
            }
        }
        List<int[]> queue = new ArrayList<>();
        queue.add(new int[]{mojaLokacija.x, mojaLokacija.y});
        udaljenostMapa[mojaLokacija.x][mojaLokacija.y] = 0;
        pomakMapa[mojaLokacija.x][mojaLokacija.y] = new int[]{0, 0};
        while (queue.size() > 0){
            int[] trenutni = queue.get(0);
            int[] tmp;
            queue.remove(0);
            if (trenutni[0] == pacman.x && trenutni[1] == pacman.y){ //provjeri je li kolac
                najblizi = trenutni;
                break;
            }
            if (trenutni[1] < mapaMaxY - 1){
                tmp = new int[]{trenutni[0], trenutni[1]+1};
                if (mapa[tmp[0]][tmp[1]] != null && !mapa[tmp[0]][tmp[1]].isZid() && udaljenostMapa[tmp[0]][tmp[1]] == -1){
                    udaljenostMapa[tmp[0]][tmp[1]] = udaljenostMapa[trenutni[0]][trenutni[1]] + 1;
                    pomakMapa[tmp[0]][tmp[1]] = new int[]{0, 1};
                    queue.add(tmp);
                }
            }
            if (trenutni[1] > 0){
                tmp = new int[]{trenutni[0], trenutni[1]-1};
                if (mapa[tmp[0]][tmp[1]] != null && !mapa[tmp[0]][tmp[1]].isZid() && udaljenostMapa[tmp[0]][tmp[1]] == -1){
                    udaljenostMapa[tmp[0]][tmp[1]] = udaljenostMapa[trenutni[0]][trenutni[1]] + 1;
                    pomakMapa[tmp[0]][tmp[1]] = new int[]{0, -1};
                    queue.add(tmp);
                }
            }
            if (trenutni[0] < mapaMaxX - 1){
                tmp = new int[]{trenutni[0]+1, trenutni[1]};
                if (mapa[tmp[0]][tmp[1]] != null && !mapa[tmp[0]][tmp[1]].isZid() && udaljenostMapa[tmp[0]][tmp[1]] == -1){
                    udaljenostMapa[tmp[0]][tmp[1]] = udaljenostMapa[trenutni[0]][trenutni[1]] + 1;
                    pomakMapa[tmp[0]][tmp[1]] = new int[]{1, 0};
                    queue.add(tmp);
                }
            }
            if (trenutni[0] > 0){
                tmp = new int[]{trenutni[0]-1, trenutni[1]};
                if (mapa[tmp[0]][tmp[1]] != null && !mapa[tmp[0]][tmp[1]].isZid() && udaljenostMapa[tmp[0]][tmp[1]] == -1){
                    udaljenostMapa[tmp[0]][tmp[1]] = udaljenostMapa[trenutni[0]][trenutni[1]] + 1;
                    pomakMapa[tmp[0]][tmp[1]] = new int[]{-1, 0};
                    queue.add(tmp);
                }
            }
        }
        int[] zeljeniPomak = null;
        if (najblizi != null){ //nađi put
            int[] trenutni = najblizi;
            int[] novi, pomak;
            while (!(pomakMapa[trenutni[0]][trenutni[1]][0] == 0 && pomakMapa[trenutni[0]][trenutni[1]][1] == 0)){
                pomak = pomakMapa[trenutni[0]][trenutni[1]];
                novi = new int[]{trenutni[0] - pomak[0], trenutni[1] - pomak[1]};
                if (pomakMapa[novi[0]][novi[1]][0] == 0 && pomakMapa[novi[0]][novi[1]][1] == 0){
                    zeljeniPomak = pomak;
                    break;
                }else{
                    trenutni = novi;
                }
            }
        }
        if (zeljeniPomak != null){
            for (int i=0;i<moves.size();i++){ //nađi odgovarajući index za potez
                if (moves.get(i)[0] == zeljeniPomak[0] && moves.get(i)[1] == zeljeniPomak[1]){
                    return i;
                }
            }
        }
        return -1;
    }

    private int idiNaNeotkriveno(List<int[]> moves){
        int[] najblizi = null;
        int[][] udaljenostMapa = new int[mapaMaxX][mapaMaxY];
        int[][][] pomakMapa = new int[mapaMaxX][mapaMaxY][2];
        for (int i=0;i<mapaMaxX;i++){
            for (int j=0;j<mapaMaxY;j++){
                udaljenostMapa[i][j] = -1;
            }
        }
        List<int[]> queue = new ArrayList<>();
        queue.add(new int[]{mojaLokacija.x, mojaLokacija.y});
        udaljenostMapa[mojaLokacija.x][mojaLokacija.y] = 0;
        pomakMapa[mojaLokacija.x][mojaLokacija.y] = new int[]{0, 0};
        while (queue.size() > 0){
            int[] trenutni = queue.get(0);
            int[] tmp;
            queue.remove(0);
            if (trenutni[0] > 0){
                tmp = new int[]{trenutni[0]-1, trenutni[1]};
                if (mapa[tmp[0]][tmp[1]] == null){ //provjeri je li rub
                    najblizi = trenutni;
                    break;
                }
                if (mapa[tmp[0]][tmp[1]] != null && !mapa[tmp[0]][tmp[1]].isZid() && udaljenostMapa[tmp[0]][tmp[1]] == -1){
                    udaljenostMapa[tmp[0]][tmp[1]] = udaljenostMapa[trenutni[0]][trenutni[1]] + 1;
                    pomakMapa[tmp[0]][tmp[1]] = new int[]{-1, 0};
                    queue.add(tmp);
                }
            }
            if (trenutni[0] < mapaMaxX - 1){
                tmp = new int[]{trenutni[0]+1, trenutni[1]};
                if (mapa[tmp[0]][tmp[1]] == null){ //provjeri je li rub
                    najblizi = trenutni;
                    break;
                }
                if (mapa[tmp[0]][tmp[1]] != null && !mapa[tmp[0]][tmp[1]].isZid() && udaljenostMapa[tmp[0]][tmp[1]] == -1){
                    udaljenostMapa[tmp[0]][tmp[1]] = udaljenostMapa[trenutni[0]][trenutni[1]] + 1;
                    pomakMapa[tmp[0]][tmp[1]] = new int[]{1, 0};
                    queue.add(tmp);
                }
            }
            if (trenutni[1] > 0){
                tmp = new int[]{trenutni[0], trenutni[1]-1};
                if (mapa[tmp[0]][tmp[1]] == null){ //provjeri je li rub
                    najblizi = trenutni;
                    break;
                }
                if (mapa[tmp[0]][tmp[1]] != null && !mapa[tmp[0]][tmp[1]].isZid() && udaljenostMapa[tmp[0]][tmp[1]] == -1){
                    udaljenostMapa[tmp[0]][tmp[1]] = udaljenostMapa[trenutni[0]][trenutni[1]] + 1;
                    pomakMapa[tmp[0]][tmp[1]] = new int[]{0, -1};
                    queue.add(tmp);
                }
            }
            if (trenutni[1] < mapaMaxY - 1){
                tmp = new int[]{trenutni[0], trenutni[1]+1};
                if (mapa[tmp[0]][tmp[1]] == null){ //provjeri je li rub
                    najblizi = trenutni;
                    break;
                }
                if (mapa[tmp[0]][tmp[1]] != null && !mapa[tmp[0]][tmp[1]].isZid() && udaljenostMapa[tmp[0]][tmp[1]] == -1){
                    udaljenostMapa[tmp[0]][tmp[1]] = udaljenostMapa[trenutni[0]][trenutni[1]] + 1;
                    pomakMapa[tmp[0]][tmp[1]] = new int[]{0, 1};
                    queue.add(tmp);
                }
            }
        }
        int[] zeljeniPomak = null;
        if (najblizi != null){ //nađi put
            int[] trenutni = najblizi;
            int[] novi, pomak;
            while (!(pomakMapa[trenutni[0]][trenutni[1]][0] == 0 && pomakMapa[trenutni[0]][trenutni[1]][1] == 0)){
                pomak = pomakMapa[trenutni[0]][trenutni[1]];
                novi = new int[]{trenutni[0] - pomak[0], trenutni[1] - pomak[1]};
                if (pomakMapa[novi[0]][novi[1]][0] == 0 && pomakMapa[novi[0]][novi[1]][1] == 0){
                    zeljeniPomak = pomak;
                    break;
                }else{
                    trenutni = novi;
                }
            }
        }
        if (zeljeniPomak != null){
            for (int i=0;i<moves.size();i++){ //nađi odgovarajući index za potez
                if (moves.get(i)[0] == zeljeniPomak[0] && moves.get(i)[1] == zeljeniPomak[1]){
                    odredisteKoord = new Coordinate(najblizi[0], najblizi[1]);
                    return i;
                }
            }
        }
        return -1;
    }

    private int idiNaRandom(List<int[]> moves){
        int[] najblizi = null;
        int[][] udaljenostMapa = new int[mapaMaxX][mapaMaxY];
        int[][][] pomakMapa = new int[mapaMaxX][mapaMaxY][2];
        for (int i=0;i<mapaMaxX;i++){
            for (int j=0;j<mapaMaxY;j++){
                udaljenostMapa[i][j] = -1;
            }
        }
        List<int[]> queue = new ArrayList<>();
        queue.add(new int[]{mojaLokacija.x, mojaLokacija.y});
        udaljenostMapa[mojaLokacija.x][mojaLokacija.y] = 0;
        pomakMapa[mojaLokacija.x][mojaLokacija.y] = new int[]{0, 0};
        List<int[]> svaPolja = new ArrayList<>();
        while (queue.size() > 0){
            int[] trenutni = queue.get(0);
            int[] tmp;
            queue.remove(0);
            svaPolja.add(trenutni);
            if (trenutni[1] < mapaMaxY - 1){
                tmp = new int[]{trenutni[0], trenutni[1]+1};
                if (mapa[tmp[0]][tmp[1]] != null && !mapa[tmp[0]][tmp[1]].isZid() && udaljenostMapa[tmp[0]][tmp[1]] == -1){
                    udaljenostMapa[tmp[0]][tmp[1]] = udaljenostMapa[trenutni[0]][trenutni[1]] + 1;
                    pomakMapa[tmp[0]][tmp[1]] = new int[]{0, 1};
                    queue.add(tmp);
                }
            }
            if (trenutni[1] > 0){
                tmp = new int[]{trenutni[0], trenutni[1]-1};
                if (mapa[tmp[0]][tmp[1]] != null && !mapa[tmp[0]][tmp[1]].isZid() && udaljenostMapa[tmp[0]][tmp[1]] == -1){
                    udaljenostMapa[tmp[0]][tmp[1]] = udaljenostMapa[trenutni[0]][trenutni[1]] + 1;
                    pomakMapa[tmp[0]][tmp[1]] = new int[]{0, -1};
                    queue.add(tmp);
                }
            }
            if (trenutni[0] < mapaMaxX - 1){
                tmp = new int[]{trenutni[0]+1, trenutni[1]};
                if (mapa[tmp[0]][tmp[1]] != null && !mapa[tmp[0]][tmp[1]].isZid() && udaljenostMapa[tmp[0]][tmp[1]] == -1){
                    udaljenostMapa[tmp[0]][tmp[1]] = udaljenostMapa[trenutni[0]][trenutni[1]] + 1;
                    pomakMapa[tmp[0]][tmp[1]] = new int[]{1, 0};
                    queue.add(tmp);
                }
            }
            if (trenutni[0] > 0){
                tmp = new int[]{trenutni[0]-1, trenutni[1]};
                if (mapa[tmp[0]][tmp[1]] != null && !mapa[tmp[0]][tmp[1]].isZid() && udaljenostMapa[tmp[0]][tmp[1]] == -1){
                    udaljenostMapa[tmp[0]][tmp[1]] = udaljenostMapa[trenutni[0]][trenutni[1]] + 1;
                    pomakMapa[tmp[0]][tmp[1]] = new int[]{-1, 0};
                    queue.add(tmp);
                }
            }
        }
        int[] odrediste = svaPolja.get(random.nextInt(svaPolja.size()));
        int[] zeljeniPomak = null;
        if (odrediste != null){ //nađi put
            int[] trenutni = odrediste;
            int[] novi, pomak;
            while (!(pomakMapa[trenutni[0]][trenutni[1]][0] == 0 && pomakMapa[trenutni[0]][trenutni[1]][1] == 0)){
                pomak = pomakMapa[trenutni[0]][trenutni[1]];
                novi = new int[]{trenutni[0] - pomak[0], trenutni[1] - pomak[1]};
                if (pomakMapa[novi[0]][novi[1]][0] == 0 && pomakMapa[novi[0]][novi[1]][1] == 0){
                    zeljeniPomak = pomak;
                    break;
                }else{
                    trenutni = novi;
                }
            }
        }
        if (zeljeniPomak != null){
            for (int i=0;i<moves.size();i++){ //nađi odgovarajući index za potez
                if (moves.get(i)[0] == zeljeniPomak[0] && moves.get(i)[1] == zeljeniPomak[1]){
                    odredisteKoord = new Coordinate(odrediste[0], odrediste[1]);
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public int decideMove(ArrayList<int[]> moves, PacmanVisibleWorld mySurroundings, WorldEntity.WorldEntityInfo myInfo) {
        //Ako je mrtav poništi sve
        if (myInfo.hasProperty(GhostAgent.deathPropertyName)){
            novaMapa();
            return 0;
        }

        //osvježi mapu s novim naučenim činjenicama
        Coordinate pacmanTmp = updateMapAndPacman(mySurroundings);

        //otkriven je pacman
        if (pacmanTmp != null){
            odredisteKoord = pacmanTmp;
        }
        //pojeli smo pacmana
        if (odredisteKoord != null && mojaLokacija.x == odredisteKoord.x && mojaLokacija.y == odredisteKoord.y){
            odredisteKoord = null;
        }

        int bestMove = -1;
        //znamo di je pacman, odredi najbolji put
        if (odredisteKoord != null){
            bestMove = idiPrema(odredisteKoord, moves);
        }
        //ne znamo di je
        if (bestMove == -1){
            if (random.nextInt(5) == 0){ //otkrivaj
                bestMove = idiNaNeotkriveno(moves);
            }else{ //idi na random mjesto
                bestMove = idiNaRandom(moves);
            }
        }
        if (bestMove == -1){
            bestMove = random.nextInt(moves.size());
        }

        mojaLokacija = new Coordinate(mojaLokacija.x + moves.get(bestMove)[0], mojaLokacija.y + moves.get(bestMove)[1]);
        return bestMove;
    }
}
