//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package hr.fer.zemris.ims.lab;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;

import hr.fer.zemris.ims.lab.PacmanAI.Location;
import hr.fer.zemris.ims.utils.AgentAI;
import hr.fer.zemris.ims.utils.PacmanVisibleWorld;
import hr.fer.zemris.ims.utils.WorldEntity;

public class PacmanAIv2 extends AgentAI {
    private HashSet<Location> powerups = new HashSet();
    private HashSet<Location> points = new HashSet();
    private Location myLocation = new Location(0, 0);
    private boolean amDead = false;
    private Location targetLocation;
    private HashSet<Location> targetRoute;
    private int targetDuration;
    private Date now;
    private Random r;

    public PacmanAIv2() {
        this.targetLocation = this.myLocation;
        this.targetRoute = new HashSet();
        this.targetDuration = 0;
        this.now = new Date();
        this.r = new Random(this.now.getTime());
    }

    public int decideMove(ArrayList<int[]> moves, PacmanVisibleWorld mySurroundings, WorldEntity.WorldEntityInfo myInfo) {
        int radiusX = mySurroundings.getDimensionX() / 2;
        int radiusY = mySurroundings.getDimensionY() / 2;
        boolean powerUP = myInfo.hasProperty("PowerUp");
        boolean wasReset = myInfo.hasProperty("Death");
        if(wasReset && !this.amDead) {
            this.amDead = true;
            this.myLocation = null;
        }

        if(!wasReset && this.amDead) {
            this.amDead = false;
        }

        if(this.targetLocation == null || this.targetLocation == this.myLocation) {
            if(this.points.iterator().hasNext()) {
                this.targetLocation = (Location)this.points.iterator().next();
            } else if(this.powerups.iterator().hasNext()) {
                this.targetLocation = (Location)this.powerups.iterator().next();
            } else {
                this.targetLocation = null;
            }

            this.targetDuration = 0;
            this.targetRoute.clear();
        }

        ++this.targetDuration;
        float targetPointDistance = this.myLocation != null && this.targetLocation != null?this.myLocation.distanceTo(this.targetLocation):3.4028235E38F;
        float ghostDistance = 3.4028235E38F;
        Location ghostLocation = null;

        int nextLocation;
        for(int currMinPDistance = -radiusX; currMinPDistance <= radiusX; ++currMinPDistance) {
            for(nextLocation = -radiusY; nextLocation <= radiusY; ++nextLocation) {
                if(currMinPDistance != 0 || nextLocation != 0) {
                    ArrayList moveIndex = mySurroundings.getWorldInfoAt((float)currMinPDistance, (float)nextLocation);
                    HashMap i = mySurroundings.getWorldMetadataAt((float)currMinPDistance, (float)nextLocation);
                    Location move;
                    if(i != null) {
                        if(this.myLocation == null) {
                            move = (Location)i.get(Integer.valueOf(myInfo.getID()));
                            if(move != null) {
                                this.myLocation = move;
                            }
                        } else {
                            move = new Location(this.myLocation.getX() + currMinPDistance, this.myLocation.getY() + nextLocation);
                            i.put(Integer.valueOf(myInfo.getID()), move);
                        }
                    }

                    if(moveIndex != null) {
                        move = this.myLocation != null?new Location(this.myLocation.getX() + currMinPDistance, this.myLocation.getY() + nextLocation):new Location(currMinPDistance, nextLocation);
                        float moveLocation = this.myLocation != null?this.myLocation.distanceTo(move):move.distanceTo(new Location(0, 0));
                        Iterator newPDistance = moveIndex.iterator();

                        while(newPDistance.hasNext()) {
                            WorldEntity.WorldEntityInfo newGDistance = (WorldEntity.WorldEntityInfo)newPDistance.next();
                            if(newGDistance.getIdentifier().compareToIgnoreCase("Pacman") != 0 && newGDistance.getIdentifier().compareToIgnoreCase("Wall") != 0) {
                                if(newGDistance.getIdentifier().compareToIgnoreCase("Point") == 0) {
                                    this.points.add(move);
                                    if(moveLocation < targetPointDistance && (this.targetLocation == null || this.targetLocation.compareTo(move) != 0)) {
                                        this.targetLocation = move;
                                        this.targetRoute.clear();
                                        this.targetDuration = 0;
                                    }
                                } else if(newGDistance.getIdentifier().compareToIgnoreCase("Powerup") == 0) {
                                    this.powerups.add(move);
                                } else if(newGDistance.getIdentifier().compareToIgnoreCase("Ghost") == 0) {
                                    if(moveLocation < ghostDistance) {
                                        ghostDistance = moveLocation;
                                        ghostLocation = move;
                                    }

                                    if(powerUP) {
                                        this.targetLocation = move;
                                        this.targetRoute.clear();
                                        this.targetDuration = 0;
                                    }
                                } else {
                                    this.printStatus("I dont know what " + newGDistance.getIdentifier() + " is!");
                                }
                            }
                        }
                    }
                }
            }
        }

        if(this.targetDuration > 10) {
            ArrayList var19 = new ArrayList(this.points);
            nextLocation = this.r.nextInt(var19.size());
            this.targetLocation = (Location)var19.get(nextLocation);
            this.targetRoute.clear();
            this.targetDuration = 0;
        }

        float var20 = 3.4028235E38F;
        Location var21 = null;
        int var22 = 0;

        for(int var23 = moves.size() - 1; var23 >= 0; --var23) {
            int[] var24 = (int[])moves.get(var23);
            Location var25 = this.myLocation != null?new Location(this.myLocation.getX() + var24[0], this.myLocation.getY() + var24[1]):new Location(var24[0], var24[1]);
            if(!this.targetRoute.contains(var25)) {
                float var26 = var25.distanceTo(this.targetLocation);
                float var27 = ghostDistance < 3.4028235E38F?var25.distanceTo(ghostLocation):3.4028235E38F;
                if(var26 <= var20 && (var27 > 1.0F || powerUP)) {
                    var20 = var26;
                    var21 = var25;
                    var22 = var23;
                }
            }
        }

        if(this.myLocation != null) {
            this.points.remove(this.myLocation);
            this.powerups.remove(this.myLocation);
            this.myLocation = var21;
        }

        this.targetRoute.add(var21);
        return var22;
    }
}
