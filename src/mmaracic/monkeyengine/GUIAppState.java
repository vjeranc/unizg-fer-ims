//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package mmaracic.monkeyengine;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.renderer.Camera;
import com.jme3.scene.Node;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.Label;
import de.lessvoid.nifty.controls.ListBox;
import de.lessvoid.nifty.controls.TextField;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import java.awt.Component;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import hr.fer.zemris.ims.utils.MessageCollector;
import hr.fer.zemris.ims.utils.PacmanConstants;
import hr.fer.zemris.ims.utils.PacmanWorld.Outcome;

public class GUIAppState extends AbstractAppState implements ScreenController {
    Properties p = new Properties();
    private SimpleApplication app;
    private Camera cam;
    private Node rootNode;
    private AssetManager assetManager;
    GameAppState game = null;
    Outcome res;
    int msgHistoryLen;
    boolean locked;
    NiftyJmeDisplay niftyDisplay;

    public GUIAppState() {
        this.res = Outcome.Ongoing;
        this.msgHistoryLen = 50;
        this.locked = false;
    }

    private void saveProperties() {
        if(this.niftyDisplay.getNifty().getCurrentScreen().getScreenId().compareTo("start") == 0) {
            TextField txtClassPath = (TextField)this.niftyDisplay.getNifty().getCurrentScreen().findNiftyControl("txtClassPath", TextField.class);
            this.p.setProperty("classPath", txtClassPath.getRealText());
            TextField txtPacman = (TextField)this.niftyDisplay.getNifty().getCurrentScreen().findNiftyControl("txtPacmanAI", TextField.class);
            this.p.setProperty("pacmanAI", txtPacman.getRealText());
            TextField txtGhost = (TextField)this.niftyDisplay.getNifty().getCurrentScreen().findNiftyControl("txtGhostAI", TextField.class);
            this.p.setProperty("ghostAI", txtGhost.getRealText());

            try {
                this.p.store(new FileOutputStream("GameSettings.properties"), "");
            } catch (IOException var5) {
                Logger.getLogger(GUIAppState.class.getName()).log(Level.SEVERE, (String)null, var5);
            }
        }

    }

    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        this.app = (SimpleApplication)app;
        this.cam = this.app.getCamera();
        this.rootNode = this.app.getRootNode();
        this.assetManager = this.app.getAssetManager();

        try {
            this.app.setDisplayStatView(false);
            this.app.setDisplayFps(false);
            this.app.getFlyByCamera().setDragToRotate(true);
            this.niftyDisplay = new NiftyJmeDisplay(this.assetManager, this.app.getInputManager(), this.app.getAudioRenderer(), this.app.getGuiViewPort());
            this.app.getGuiViewPort().addProcessor(this.niftyDisplay);
            Nifty ex = this.niftyDisplay.getNifty();
            ex.validateXml("Interface/niftyGUIMain.xml");
            ex.fromXml("Interface/niftyGUIMain.xml", "start", new ScreenController[]{this});
            ex.validateXml("Interface/niftyGUIOverlay.xml");
            ex.addXml("Interface/niftyGUIOverlay.xml");
            ex.validateXml("Interface/niftyGUIEnd.xml");
            ex.addXml("Interface/niftyGUIEnd.xml");
            ex.addXml("Interface/niftyGUIRegister.xml");
        } catch (Exception var4) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Error in GUI XML: " + var4.getMessage());
            this.app.getGuiViewPort().removeProcessor(this.niftyDisplay);
            this.app.setDisplayStatView(false);
            this.app.setDisplayFps(true);
            this.app.getFlyByCamera().setDragToRotate(false);
        }

    }

    public void update(float tpf) {
        ListBox outcome;
        if(this.niftyDisplay.getNifty().getCurrentScreen().getScreenId().compareTo("overlay") == 0) {
            ArrayList tempRes = MessageCollector.getMessages();
            if(!tempRes.isEmpty()) {
                outcome = (ListBox)this.niftyDisplay.getNifty().getCurrentScreen().findNiftyControl("lstMessages", ListBox.class);

                for(Iterator softOutcome = tempRes.iterator(); softOutcome.hasNext(); outcome.selectItem(Integer.valueOf(outcome.itemCount() - 1))) {
                    String softState = (String)softOutcome.next();
                    outcome.addItem(softState);
                    if(outcome.itemCount() > this.msgHistoryLen) {
                        outcome.removeItemByIndex(0);
                    }
                }
            }
        }

        if(this.game != null) {
            Outcome tempRes1 = this.game.getGameState();
            if(tempRes1 != Outcome.Ongoing) {
                outcome = (ListBox)this.niftyDisplay.getNifty().getCurrentScreen().findNiftyControl("lstMessages", ListBox.class);
                outcome.clear();
                this.res = tempRes1;
                this.niftyDisplay.getNifty().gotoScreen("end");
            }

            Label outcome1;
            if(this.niftyDisplay.getNifty().getCurrentScreen().getScreenId().compareTo("overlay") == 0) {
                if(this.game.isPaused()) {
                    outcome1 = (Label)this.niftyDisplay.getNifty().getCurrentScreen().findNiftyControl("alert", Label.class);
                    outcome1.setText("Paused");
                } else {
                    outcome1 = (Label)this.niftyDisplay.getNifty().getCurrentScreen().findNiftyControl("alert", Label.class);
                    outcome1.setText("Game in progress");
                }
            }

            if(this.niftyDisplay.getNifty().getCurrentScreen().getScreenId().compareTo("end") == 0) {
                outcome1 = (Label)this.niftyDisplay.getNifty().getScreen("end").findNiftyControl("labOutcome", Label.class);
                Element softOutcome1 = this.niftyDisplay.getNifty().getScreen("end").findElementByName("labSoftOutcome");
                switch(this.res) {
                case Win:
                    outcome1.setText("Win");
                    break;
                case Defeat:
                    outcome1.setText("Defeat");
                    break;
                default:
                    outcome1.setText("Tie");
                }

                Map softState1 = this.game.getSoftGameState();
                StringBuilder softInfo = new StringBuilder();
                Iterator i$ = softState1.keySet().iterator();

                while(i$.hasNext()) {
                    String prop = (String)i$.next();
                    softInfo.append(prop + ": ");
                    softInfo.append((String)softState1.get(prop) + " ");
                }

                ((TextRenderer)softOutcome1.getRenderer(TextRenderer.class)).setText(softInfo.toString());
                this.game = null;
                this.res = Outcome.Ongoing;
            }
        }

    }

    public void cleanup() {
        super.cleanup();
        this.saveProperties();
        this.app.getGuiViewPort().removeProcessor(this.niftyDisplay);
        this.app.setDisplayStatView(false);
        this.app.setDisplayFps(true);
        this.app.getFlyByCamera().setDragToRotate(false);
    }

    public void bind(Nifty nifty, Screen screen) {
    }

    public void onStartScreen() {
        try {
            this.p.load(new FileInputStream("GameSettings.properties"));
            String ex = this.p.getProperty("classPath");
            if(ex != null) {
                TextField pacmanAI = (TextField)this.niftyDisplay.getNifty().getCurrentScreen().findNiftyControl("txtClassPath", TextField.class);
                pacmanAI.setText(ex);
            }

            String pacmanAI1 = this.p.getProperty("pacmanAI");
            if(pacmanAI1 != null) {
                TextField ghostAI = (TextField)this.niftyDisplay.getNifty().getCurrentScreen().findNiftyControl("txtPacmanAI", TextField.class);
                ghostAI.setText(pacmanAI1);
            }

            String ghostAI1 = this.p.getProperty("ghostAI");
            if(ghostAI1 != null) {
                TextField txtGhost = (TextField)this.niftyDisplay.getNifty().getCurrentScreen().findNiftyControl("txtGhostAI", TextField.class);
                txtGhost.setText(ghostAI1);
            }
        } catch (FileNotFoundException var5) {
            Logger.getLogger(GUIAppState.class.getName()).log(Level.SEVERE, (String)null, var5);
        } catch (IOException var6) {
            Logger.getLogger(GUIAppState.class.getName()).log(Level.SEVERE, (String)null, var6);
        }

    }

    public void onEndScreen() {
    }

    public void startGame() {
        TextField txtClassPath = (TextField)this.niftyDisplay.getNifty().getCurrentScreen().findNiftyControl("txtClassPath", TextField.class);
        String classPath = txtClassPath.getRealText();
        TextField txtPacman = (TextField)this.niftyDisplay.getNifty().getCurrentScreen().findNiftyControl("txtPacmanAI", TextField.class);
        String pacmanAI = txtPacman.getRealText();
        TextField txtGhost = (TextField)this.niftyDisplay.getNifty().getCurrentScreen().findNiftyControl("txtGhostAI", TextField.class);
        String ghostAI = txtGhost.getRealText();
        if(!this.locked) {
            this.game = new GameAppState(PacmanConstants.PACMAN_BUILDER, 20, 20, PacmanConstants.refreshRate, classPath, pacmanAI, ghostAI);
        } else {
            this.game = new GameAppState(PacmanConstants.PACMAN_BUILDER, 20, 20, PacmanConstants.refreshRate, classPath, pacmanAI, ghostAI);
        }

        this.saveProperties();
        this.niftyDisplay.getNifty().gotoScreen("overlay");
        this.res = Outcome.Ongoing;
        this.app.getStateManager().attach(this.game);
    }

    public void mainMenu() {
        this.niftyDisplay.getNifty().gotoScreen("start");
    }

    public void quitGame() {
        this.app.stop();
    }

    public void selectPath() {
        TextField txtClassPath = (TextField)this.niftyDisplay.getNifty().getCurrentScreen().findNiftyControl("txtClassPath", TextField.class);
        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Select Class Path Folder");
        fc.setFileSelectionMode(1);
        if(fc.showDialog((Component)null, "Select") == 0) {
            File path = fc.getSelectedFile();
            txtClassPath.setText("file:" + path.getAbsolutePath() + File.separator);
        }

    }

    public void registerMenu() {
    }

    public void registerUser() {
    }

    public void submitAI() {
    }
}
