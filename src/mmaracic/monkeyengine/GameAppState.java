//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package mmaracic.monkeyengine;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.input.InputManager;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.Trigger;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Node;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import hr.fer.zemris.ims.utils.PacmanConstants;
import hr.fer.zemris.ims.utils.PacmanWorld;
import hr.fer.zemris.ims.utils.WorldEntity;
import hr.fer.zemris.ims.utils.WorldFactory;
import hr.fer.zemris.ims.utils.PacmanWorld.Outcome;

public class GameAppState extends AbstractAppState {
    private boolean pause = false;
    private Outcome done;
    private final String PAUSE_MAPPING;
    private SimpleApplication app;
    private Camera cam;
    private Node rootNode;
    private AssetManager assetManager;
    private InputManager input;
    private Node gameNode;
    PacmanWorld bw;
    GregorianCalendar gcal;
    ActionListener al;
    private String classPath;
    private String pacmanAIClass;
    private String ghostAIClass;
    private String WorldBuilderClass;
    private int worldTimeStepMs;
    private int worldWidth;
    private int worldHeight;

    public GameAppState() {
        this.done = Outcome.Ongoing;
        this.PAUSE_MAPPING = "Pause";
        this.gameNode = new Node("gameNode");
        this.bw = null;
        this.gcal = null;
        this.al = null;
        this.classPath = null;
        this.pacmanAIClass = PacmanConstants.DEFAULT_PACMAN_AI;
        this.ghostAIClass = PacmanConstants.DEFAULT_GHOST_AI;
        this.WorldBuilderClass = PacmanConstants.PACMAN_BUILDER;
        this.worldTimeStepMs = 5;
        this.worldWidth = 20;
        this.worldHeight = 20;
    }

    public GameAppState(String WorldBuilderClass, int worldWidth, int worldHeight, int worldTimeStepMs, String classPath, String pacmanAIClass, String ghostAIClass) {
        this.done = Outcome.Ongoing;
        this.PAUSE_MAPPING = "Pause";
        this.gameNode = new Node("gameNode");
        this.bw = null;
        this.gcal = null;
        this.al = null;
        this.classPath = null;
        this.pacmanAIClass = PacmanConstants.DEFAULT_PACMAN_AI;
        this.ghostAIClass = PacmanConstants.DEFAULT_GHOST_AI;
        this.WorldBuilderClass = PacmanConstants.PACMAN_BUILDER;
        this.worldTimeStepMs = 5;
        this.worldWidth = 20;
        this.worldHeight = 20;
        if(WorldBuilderClass != null && WorldBuilderClass.compareToIgnoreCase("") != 0) {
            this.WorldBuilderClass = WorldBuilderClass;
        }

        if(worldWidth >= 10) {
            this.worldWidth = worldWidth;
        }

        if(worldHeight >= 10) {
            this.worldHeight = worldHeight;
        }

        if(worldTimeStepMs > 0) {
            this.worldTimeStepMs = worldTimeStepMs;
        }

        if(classPath != null && classPath.compareToIgnoreCase("") != 0) {
            this.classPath = classPath;
        }

        if(pacmanAIClass != null && pacmanAIClass.compareToIgnoreCase("") != 0) {
            this.pacmanAIClass = pacmanAIClass;
        }

        if(ghostAIClass != null && ghostAIClass.compareToIgnoreCase("") != 0) {
            this.ghostAIClass = ghostAIClass;
        }

        this.gcal = new GregorianCalendar();
    }

    public boolean isPaused() {
        return this.pause;
    }

    public Outcome getGameState() {
        return this.done;
    }

    public Map<String, String> getSoftGameState() {
        HashMap state = new HashMap();
        state.put("Points Total", Integer.toString(this.bw.getNoTotalPoints()));
        state.put("Points Eaten", Integer.toString(this.bw.getPointsEaten()));
        return state;
    }

    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        this.app = (SimpleApplication)app;
        this.cam = this.app.getCamera();
        this.rootNode = this.app.getRootNode();
        this.assetManager = this.app.getAssetManager();
        this.input = this.app.getInputManager();
        this.gcal = new GregorianCalendar();
        HashMap builderParams = new HashMap();
        builderParams.put("classPath", this.classPath);
        builderParams.put("pacmanAI", this.pacmanAIClass);
        builderParams.put("ghostAI", this.ghostAIClass);
        builderParams.put("noGhosts", Integer.toString(4));
        builderParams.put("pitDimX", Integer.toString(4));
        builderParams.put("pitDimY", Integer.toString(4));
        KeyTrigger pauseTrigger = new KeyTrigger(25);
        this.input.addMapping("Pause", new Trigger[]{pauseTrigger});
        this.al = new ActionListener() {
            public void onAction(String name, boolean isPressed, float tpf) {
                if(name.equalsIgnoreCase("Pause") && !isPressed) {
                    GameAppState.this.pause = !GameAppState.this.pause;
                }

            }
        };
        this.input.addListener(this.al, new String[]{"Pause"});
        this.bw = WorldFactory.createWorld(this.WorldBuilderClass, this.worldWidth, this.worldHeight, 1, this.worldTimeStepMs, builderParams, this.assetManager);
        HashSet entities = this.bw.getEntities();
        Iterator i$ = entities.iterator();

        while(i$.hasNext()) {
            WorldEntity we = (WorldEntity)i$.next();
            this.gameNode.attachChild(we.getSpatial());
        }

        this.rootNode.attachChild(this.gameNode);
        this.app.setDisplayStatView(false);
        this.app.setDisplayFps(true);
        this.cam.setLocation(new Vector3f(0.0F, 0.0F, 25.0F));
    }

    public void update(float tpf) {
        if(!this.pause) {
            GregorianCalendar now = new GregorianCalendar();
            if(now.compareTo(this.gcal) > 0) {
                this.done = this.bw.processAgents();
                if(this.done != Outcome.Ongoing) {
                    this.app.getStateManager().detach(this);
                }

                this.gcal = now;
                this.gcal.add(14, this.worldTimeStepMs);
            }
        }

    }

    public void cleanup() {
        super.cleanup();
        this.input.removeListener(this.al);
        this.al = null;
        this.input.deleteMapping("Pause");
        int detachWorld = this.rootNode.detachChildNamed("gameNode");
        if(detachWorld == -1) {
            Logger.getLogger(GameAppState.class.getName()).log(Level.SEVERE, "Pacman world deletion failed!");
        }

        this.app.setDisplayFps(false);
        this.cam.setLocation(new Vector3f(0.0F, 0.0F, 10.0F));
    }
}
